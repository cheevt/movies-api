<?php

use Faker\Generator as Faker;

$factory->define(App\Movie::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'director' => $faker->firstName . ' ' . $faker->lastName,
        'imageUrl' => $faker->imageUrl,
        'duration' => $faker->numberBetween(60, 360),
        'releaseDate' => $faker->date('Y-m-d'),
        'genre' => $faker->name
    ];
});
